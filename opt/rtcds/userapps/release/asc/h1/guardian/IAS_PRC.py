# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#  IAS_PRC.py
#
# $Id: IAS_PRC.py 9158 2014-11-18 23:27:56Z alexan.staley@LIGO.ORG $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/asc/h1/guardian/IAS_PRC.py $
#

from guardian import GuardState, NodeManager

from ISC_library import \
			get_subordinate_watchdog_check_decorator,\
			assert_mc_locked,\
			assert_prx_locked

from ISC_GEN_STATES import \
			gen_REFL_WFS_CENTERING,\
			gen_CORNER_WFS_CENTERING,\
			gen_OFFLOAD_ALIGNMENT

from ISC_DOF import\
			DOWN,\
			gen_LOCK_DOF


#################################################
# n o d e s
##################################################
nodes = NodeManager(['LSC_CONFIGS'])


#################################################
# s t a t e s
##################################################

LOCK_PRX = gen_LOCK_DOF()
LOCK_PRX.dof = 'PRX'

class LOCK_PRMI(GuardState): 
    request = False
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    @assert_mc_locked
    def main(self):
        nodes['LSC_CONFIGS'] = 'PRMI_SB_OFFLOADED'
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    @assert_mc_locked
    def run(self):
        if nodes.arrived:
            return True
        else:
            notify('WAITING for ISC_CONFIG')

#################################################
## states to take care of WFS centering
#################################################

REFL_WFS_CENTERING_XARM = gen_REFL_WFS_CENTERING('XARM')
REFL_WFS_CENTERING_YARM = gen_REFL_WFS_CENTERING('YARM')
REFL_WFS_CENTERING_PRX = gen_REFL_WFS_CENTERING('PRX')
REFL_WFS_CENTERING_SRY = gen_REFL_WFS_CENTERING('SRY')

CORNER_WFS_CENTERING_PRMI = gen_CORNER_WFS_CENTERING('PRMI')

#################################################
## Align PRM using refl WFS and PRX
class PRM_ALIGN(GuardState):
    request = True
    @assert_mc_locked
    @assert_prx_locked
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        #REFL B 9 I to PRC1  
        for jj in range(1,28):
            ezca['ASC-INMATRIX_P_3_%d'%jj] = 0
            ezca['ASC-INMATRIX_Y_3_%d'%jj] = 0
        ezca['ASC-INMATRIX_P_3_13'] = 1
        ezca['ASC-INMATRIX_Y_3_13'] = 1
        # control filters
        ezca.get_LIGOFilter('ASC-PRC1_P').only_on('FM1', 'FM2', 'OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-PRC1_Y').only_on('FM1', 'FM2', 'OUTPUT', 'DECIMATION')
        ezca['ASC-PRC1_P_GAIN'] = -0.003
        ezca['ASC-PRC1_Y_GAIN'] = 0.03
        #output matrix
        ezca['ASC-OUTMATRIX_P_1_3'] = 1
        ezca['ASC-OUTMATRIX_Y_1_3'] = 1 
        #turn on servos
        ezca.get_LIGOFilter('ASC-PRC1_P').switch_on('INPUT')
        ezca.get_LIGOFilter('ASC-PRC1_Y').switch_on('INPUT')

OFFLOAD_PRM_ALIGNMENT = gen_OFFLOAD_ALIGNMENT('PRX')
OFFLOAD_PRM_ALIGNMENT.optics = ['PRM']
OFFLOAD_PRM_ALIGNMENT.loops = ['PRC1_P', 'PRC1_Y']

##################################################

edges = [
# PRX edges:
    ('DOWN', 'LOCK_PRX'),
    ('LOCK_PRX', 'REFL_WFS_CENTERING_PRX'), 
    ('REFL_WFS_CENTERING_PRX', 'PRM_ALIGN'),
    ('PRM_ALIGN', 'OFFLOAD_PRM_ALIGNMENT'), 
]
