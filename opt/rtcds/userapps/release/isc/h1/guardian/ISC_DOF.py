# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: ISC_DOF.py 9911 2015-02-25 11:46:04Z kiwamu.izumi@LIGO.ORG $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/isc/h1/guardian/ISC_DOF.py $

from guardian import GuardState, GuardStateDecorator
from guardian.ligopath import userapps_path
from guardian import NodeManager
from subprocess import check_output
import cdsutils
import time
from math import sqrt, pow

##################################################
import lscparams as lscparams

from ISC_library import *
from ISC_GEN_STATES import *


##################################################
ca_monitor = False
ca_monitor_notify = False
##################################################

#TODO: DRMI lock threshold input power scaling
#TODO: Power scaling for other thresholds
#TODO: DC readout transition
#TODO: Turn on WFS once at zero carm offset
#TODO: Find carrier resonance in OMC length


###################################################


##################################################
## filter names
##################################################
#from ezca import Ezca as ezca_dummy
#prcl_filt = ezca.get_LIGOFilter('LSC-PRCL')
#mich_filt = ezca.get_LIGOFilter('LSC-MICH')
#srcl_filt = ezca_dummy().get_LIGOFilter('LSC-SRCL')
#arm_filt = ezca.get_LIGOFilter('LSC-%sARM'%arm)
#prm_m2_filt = ezca.get_LIGOFilter('SUS-PRM_M2_LOCK_L')
#srm_m2_filt = ezca.get_LIGOFilter('SUS-SRM_M2_LOCK_L')


##################################################
# NODES
##################################################

nodes = NodeManager(['LSC_CONFIGS'])

##################################################
# STATES
##################################################

# FIXME: this needs to check state and move to the right place on the
# graph
class INIT(GuardState):
    request = True
    def main(self):
         print 'hello'
         log("initializing subordinate nodes...")
         nodes.set_managed()
    def run(self):
         return True



# reset everything to the good values for acquistion.  These values
# are stored in the down script.
class DOWN(GuardState):
    goto = True
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        #turn off ASC
        ezca.get_LIGOFilter('ASC-INP1_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-INP1_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-INP2_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-INP2_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-PRC1_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-PRC1_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-SRC1_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-SRC1_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-SRC2_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-SRC2_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-MICH_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-MICH_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC1_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC2_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC1_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC2_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC3_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC4_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC3_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC4_Y').switch_off('INPUT')

        # clear histories
        ezca['SUS-PRM_M1_LOCK_P_RSET'] = 2
        ezca['SUS-PRM_M1_LOCK_Y_RSET'] = 2
        ezca['ASC-PRC1_P_RSET'] = 2
        ezca['ASC-PRC1_Y_RSET'] = 2


		# turn off SRCL
	#	srcl_filt().ramp_gain(0, ramp_time=3, wait=False)

        #nodes['LSC_CONFIGS'] = 'DOWN'

    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        return True

class SET_PRM_SRM(GuardState):
    request = False
    #state that sets up feedback to PRM, SRM, which is different for inital alignment than for DRMi alignment
    def main(self):
        # turn off PRM feedback to M2
        ezca['SUS-PRM_M2_LOCK_OUTSW_P'] = 0 
        ezca['SUS-PRM_M2_LOCK_OUTSW_Y'] = 0 
        # turn on PRM feedback to M1
        ezca.switch('SUS-PRM_M1_LOCK_P', 'INPUT', 'OUTPUT', 'ON') 
        ezca.switch('SUS-PRM_M1_LOCK_Y', 'INPUT', 'OUTPUT', 'ON')
        # turn off SRM feedback to M2
        ezca['SUS-SRM_M2_LOCK_OUTSW_P'] = 0 
        ezca['SUS-SRM_M2_LOCK_OUTSW_Y'] = 0 
        # turn on SRM feedback to M1
        ezca.switch('SUS-SRM_M1_LOCK_P', 'INPUT', 'OUTPUT', 'ON') 
        ezca.switch('SUS-SRM_M1_LOCK_Y', 'INPUT', 'OUTPUT', 'ON')
        # turn on SR2 feedback to M1
        ezca.switch('SUS-SR2_M1_LOCK_P', 'INPUT', 'OUTPUT', 'ON') 
        ezca.switch('SUS-SR2_M1_LOCK_Y', 'INPUT', 'OUTPUT', 'ON')
        # Reset the PRC/SRC WFS filters
        ezca['ASC-PRC1_P_RSET'] = 2 
        ezca['ASC-PRC1_Y_RSET'] = 2 
        ezca['ASC-SRC1_P_RSET'] = 2 
        ezca['ASC-SRC1_Y_RSET'] = 2 
        return True


##################################################
# LOCK DOF uing ISC configs
##################################################
def gen_LOCK_DOF():
    class LOCK_DOF(GuardState): 
        request = False
        @get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        @assert_mc_locked
        def main(self):
            nodes['LSC_CONFIGS'] = '%s_LOCKED'%self.dof

        @get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        @assert_mc_locked
        def run(self):
            if nodes.arrived:
                return True
            else:
                notify('WAITING for LSC_CONFIG')
    return LOCK_DOF

LOCK_XARM_IR = gen_LOCK_DOF()
LOCK_XARM_IR.dof = 'XARM_IR'
LOCK_YARM_IR = gen_LOCK_DOF()
LOCK_YARM_IR.dof = 'YARM_IR'
LOCK_PRX = gen_LOCK_DOF()
LOCK_PRX.dof = 'PRX'
LOCK_SRY = gen_LOCK_DOF()
LOCK_SRY.dof = 'SRY'

class LOCK_PRMI(GuardState): 
    request = False
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    @assert_mc_locked
    def main(self):
        nodes['LSC_CONFIGS'] = 'PRMI_SB_OFFLOADED'
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    @assert_mc_locked
    def run(self):
        if nodes.arrived:
            return True
        else:
            notify('WAITING for ISC_CONFIG')

#############################
## states to take care of WFS centering
###################################

REFL_WFS_CENTERING_XARM = gen_REFL_WFS_CENTERING('XARM')
REFL_WFS_CENTERING_YARM = gen_REFL_WFS_CENTERING('YARM')
REFL_WFS_CENTERING_PRX = gen_REFL_WFS_CENTERING('PRX')
REFL_WFS_CENTERING_SRY = gen_REFL_WFS_CENTERING('SRY')

CORNER_WFS_CENTERING_PRMI = gen_CORNER_WFS_CENTERING('PRMI')

#######################################
## Align PRM using refl WFS and PRX
class PRM_ALIGN(GuardState):
    request = True
    @assert_mc_locked
    @assert_prx_locked
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        #REFL B 9 I to PRC1  
        for jj in range(1,28):
            ezca['ASC-INMATRIX_P_3_%d'%jj] = 0
            ezca['ASC-INMATRIX_Y_3_%d'%jj] = 0
        ezca['ASC-INMATRIX_P_3_13'] = 1
        ezca['ASC-INMATRIX_Y_3_13'] = 1
        # control filters
        ezca.get_LIGOFilter('ASC-PRC1_P').only_on('FM2', 'FM3', 'OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-PRC1_Y').only_on('FM2', 'FM3', 'OUTPUT', 'DECIMATION')
        ezca['ASC-PRC1_P_GAIN'] = -8
        ezca['ASC-PRC1_Y_GAIN'] = 12  #gains doubled Feb 10 2015
        #output matrix
        ezca['ASC-OUTMATRIX_P_1_3'] = 1
        ezca['ASC-OUTMATRIX_Y_1_3'] = 1 
        #turn on servos
        ezca.get_LIGOFilter('ASC-PRC1_P').switch_on('INPUT')
        ezca.get_LIGOFilter('ASC-PRC1_Y').switch_on('INPUT')



#######################################
## Align SRM using refl WFS and SRX

class SRM_ALIGN(GuardState):
    request = True
    @assert_mc_locked
    @assert_sry_locked
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        # input matrix
        clear_asc_input_matrix()
        ezca['ASC-INMATRIX_P_12_17'] = 1
        ezca['ASC-INMATRIX_Y_12_17'] = 1
        ezca['ASC-INMATRIX_P_13_18'] = 1
        ezca['ASC-INMATRIX_Y_13_18'] = 1
        #REFL 9 I to SRC1        
        ezca['ASC-INMATRIX_P_6_9'] = 1
        ezca['ASC-INMATRIX_Y_6_9'] = 1
        #AS_C 9 to SRC2        
        ezca['ASC-INMATRIX_P_7_27'] = 1
        ezca['ASC-INMATRIX_Y_7_27'] = 1
        # control filters
        ezca.get_LIGOFilter('ASC-SRC1_P').only_on('FM2', 'FM3', 'OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-SRC1_Y').only_on('FM2', 'FM3', 'OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-SRC2_P').only_on('FM2', 'FM3','FM6', 'OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-SRC2_Y').only_on('FM2', 'FM3', 'FM6','OUTPUT', 'DECIMATION')
        ezca['ASC-SRC1_P_GAIN'] = 2.7
        ezca['ASC-SRC1_Y_GAIN'] = 4.5
        ezca['ASC-SRC2_P_GAIN'] = 1
        ezca['ASC-SRC2_Y_GAIN'] = 1
        #output matrix
        ezca['ASC-OUTMATRIX_P_9_6'] = 1
        ezca['ASC-OUTMATRIX_Y_9_6'] = 1 
        ezca['ASC-OUTMATRIX_P_11_6'] = 0
        ezca['ASC-OUTMATRIX_Y_11_6'] = 0 
        ezca['ASC-OUTMATRIX_P_10_7'] = 1
        ezca['ASC-OUTMATRIX_Y_10_7'] = 1 
        ezca['ASC-OUTMATRIX_P_9_7'] = -7.08
        ezca['ASC-OUTMATRIX_Y_9_7'] = 7.12
        ezca.get_LIGOFilter('ASC-SRC1_P').switch_on('INPUT')
        ezca.get_LIGOFilter('ASC-SRC1_Y').switch_on('INPUT')
        ezca.get_LIGOFilter('ASC-SRC2_P').switch_on('INPUT')
        ezca.get_LIGOFilter('ASC-SRC2_Y').switch_on('INPUT')
   

#######################################
## Align input pointing  IM4 + pR2 to to X arm using refl WFS
class SET_INPUT_ALIGN(GuardState):
    request = False
    @assert_mc_locked
    @assert_xarm_IR_locked
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        ezca.switch('SUS-PR2_M1_LOCK_P','OUTPUT','INPUT','ON') 
        ezca.switch('SUS-PR2_M1_LOCK_Y','OUTPUT','INPUT','ON')
        # input matrix
        for jj in range(1, 28):
            ezca['ASC-INMATRIX_P_1_%d'%jj] = 0 
            ezca['ASC-INMATRIX_P_2_%d'%jj] = 0 
            ezca['ASC-INMATRIX_Y_1_%d'%jj] = 0 
            ezca['ASC-INMATRIX_Y_2_%d'%jj] = 0
        ezca['ASC-INMATRIX_P_1_9'] = 1
        ezca['ASC-INMATRIX_Y_1_9'] = 1
        ezca['ASC-INMATRIX_P_2_13'] = 1
        ezca['ASC-INMATRIX_Y_2_13'] = 1
        # control filters
        ezca.get_LIGOFilter('ASC-INP1_P').only_on('FM2', 'FM3', 'OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-INP1_Y').only_on('FM2', 'FM3', 'OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-INP2_P').only_on('FM2', 'FM3', 'OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-INP2_Y').only_on('FM2', 'FM3', 'OUTPUT', 'DECIMATION')
        ezca['ASC-INP1_P_GAIN'] = 50
        ezca['ASC-INP1_Y_GAIN'] = -50
        ezca['ASC-INP2_P_GAIN'] = -0.7
        ezca['ASC-INP2_Y_GAIN'] = -0.7
        #output matrix
        for jj in range(1, 25):
            ezca['ASC-OUTMATRIX_P_2_%d'%jj] = 0
            ezca['ASC-OUTMATRIX_P_15_%d'%jj] = 0
            ezca['ASC-OUTMATRIX_Y_2_%d'%jj] = 0
            ezca['ASC-OUTMATRIX_Y_15_%d'%jj] = 0
        ezca['ASC-OUTMATRIX_P_2_2'] = 1
        ezca['ASC-OUTMATRIX_P_15_2'] = -125
        ezca['ASC-OUTMATRIX_P_15_1'] = 1
        ezca['ASC-OUTMATRIX_Y_2_2'] = 1
        ezca['ASC-OUTMATRIX_Y_15_2'] = 40
        ezca['ASC-OUTMATRIX_Y_15_1'] = 1
        
        
    @assert_mc_locked
    @assert_xarm_IR_locked
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if not ASC_DC_centering_servos_OK():
            notify('REFL WFS DC centering')
            return 'REFL_WFS_CENTERING_XARM'
        else:
            return True

class INPUT_ALIGN(GuardState):
    request = True
    @assert_mc_locked
    @assert_xarm_IR_locked
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        if not ASC_DC_centering_servos_OK():
            notify('REFL WFS DC centering')
            ezca.get_LIGOFilter('ASC-DC1_P').switch_off('INPUT')
            ezca.get_LIGOFilter('ASC-DC1_Y').switch_off('INPUT')
            ezca.get_LIGOFilter('ASC-DC2_P').switch_off('INPUT')
            ezca.get_LIGOFilter('ASC-DC2_Y').switch_off('INPUT')
            return 'SET_INPUT_ALIGN'
        ezca.get_LIGOFilter('ASC-INP1_P').switch_on('INPUT')
        ezca.get_LIGOFilter('ASC-INP1_Y').switch_on('INPUT')
        ezca.get_LIGOFilter('ASC-INP2_P').switch_on('INPUT')
        ezca.get_LIGOFilter('ASC-INP2_Y').switch_on('INPUT')  

    @assert_mc_locked
    @assert_xarm_IR_locked
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
	def run(self):
        if not ASC_DC_centering_servos_OK():
            notify('REFL WFS DC centering')
            ezca.get_LIGOFilter('ASC-DC1_P').switch_off('INPUT')
            ezca.get_LIGOFilter('ASC-DC1_Y').switch_off('INPUT')
            ezca.get_LIGOFilter('ASC-DC2_P').switch_off('INPUT')
            ezca.get_LIGOFilter('ASC-DC2_Y').switch_off('INPUT')
			return 'SET_INPUT_ALIGN'
		else:	
			return True

class OFFLOAD_INPUT_ALIGN(GuardState):
    request = False
    @assert_mc_locked
    @assert_xarm_IR_locked
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        #offload
        offload('IM4', 'M1')
        offload('PR2', 'M1')
        # turn off the loops
        ezca.get_LIGOFilter('ASC-INP1_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-INP1_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-INP2_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-INP2_Y').switch_off('INPUT')
        #clear history
        ezca['ASC-INP1_P_RSET'] = 2
        ezca['ASC-INP1_Y_RSET'] = 2
        ezca['ASC-INP2_P_RSET'] = 2
        ezca['ASC-INP2_Y_RSET'] = 2
        notify('Save alignments of PR2 +IM4')
        return True

def gen_ALIGN_SLIDER_SERVO(dof):
    class ALIGN_SLIDER_SERVO(GuardState):
        request = True
        @assert_mc_locked
        @assert_dof_locked_gen(dof)
        @get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def main(self):
            # time constant for the running average of the error signals
            self.T = 2*numpy.pi*10 
            # initial error values (used for termination condition)
            self.average_err = numpy.zeros(len(self.errors))
            for i in range(len(self.errors)):
                # get error signal value
                    errsig = ezca[self.errors[i]]
                    self.average_err[i] = errsig
            # compute the maximum of error signal absolute values
            self.max_err = max(abs(self.average_err))
            # set ramp times
            for i in range(len(self.actuators)):
                ezca[self.actuators[i] +'_TRAMP'] = 1                

        @assert_mc_locked
        @assert_dof_locked_gen(dof)
        @get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def run(self):
            t0 = time.time()
            # compute new maximum value of error signal
            self.max_err = max(abs(self.average_err))
            log(self.max_err)
            # wait a little bit
            time.sleep(0.01)
            # Main loop, continue until error signals are all smaller than threshold. Stop if power drops below threshold
            if self.max_err > self.err_threshold and ezca[self.powersig] > self.threshold:
                # compute the time since the last actuation, to maintain uniform gain
                dt = time.time() - t0
                # loop over all error signals
                for i in range(len(self.errors)):
                    # get error signal value
                    errsig = ezca[self.errors[i]]
                    # compute new position and apply it
                    ezca[self.actuators[i] + '_OFFSET'] = ezca[self.actuators[i] + '_OFFSET'] + self.gains[i] * dt * errsig
                    # running average on error signal value
                    self.average_err[i] = (1 - 1./self.T) * self.average_err[i] + 1./self.T * ezca[self.errors[i]]*errsig
            else:
                return True
    return ALIGN_SLIDER_SERVO



YARM_ALIGN_IR = gen_ALIGN_SLIDER_SERVO('YARM')
###### PARAMETERS ######
# list of the error signals
YARM_ALIGN_IR.errors = ('ASC-REFL_A_RF9_I_PIT_OUTMON', 'ASC-REFL_A_RF9_I_YAW_OUTMON')#, 'ASC-REFL_B_RF9_I_PIT_OUTMON', 'ASC-REFL_B_RF9_I_YAW_OUTMON')
# list of corresponding actuation points
YARM_ALIGN_IR.actuators = ('SUS-ITMY_M0_OPTICALIGN_P', 'SUS-ITMY_M0_OPTICALIGN_Y')#, 'SUS-ETMY_M0_OPTICALIGN_P', 'SUS-ETMY_M0_OPTICALIGN_Y')
# loop gains 
YARM_ALIGN_IR.gains = 2*[0.01] # [-0.001, -0.001, -0.001, 0.001]
# power signal used to trigger
YARM_ALIGN_IR.powersig = 'LSC-Y_TR_A_LF_OUTMON'
YARM_ALIGN_IR.threshold = 1   # do something only if power is larger than this
YARM_ALIGN_IR.err_threshold = 1
  # stop when all error signals are below this value


OFFLOAD_SRM_ALIGNMENT = gen_OFFLOAD_ALIGNMENT('SRY')
OFFLOAD_SRM_ALIGNMENT.optics = ['SRM','SR2']
OFFLOAD_SRM_ALIGNMENT.stage = ['M1','M1']
OFFLOAD_SRM_ALIGNMENT.loops = ['SRC1_P', 'SRC1_Y','SRC2_P','SRC2_Y']

OFFLOAD_SR2_ALIGNMENT = gen_OFFLOAD_ALIGNMENT('SRY')
OFFLOAD_SR2_ALIGNMENT.optics = ['SR2']
OFFLOAD_SR2_ALIGNMENT.stage = ['M1']
OFFLOAD_SR2_ALIGNMENT.loops = ['SRC2_P', 'SRC2_Y']

OFFLOAD_PRM_ALIGNMENT = gen_OFFLOAD_ALIGNMENT('PRX')
OFFLOAD_PRM_ALIGNMENT.optics = ['PRM']
OFFLOAD_PRM_ALIGNMENT.stage = ['M1']
OFFLOAD_PRM_ALIGNMENT.loops = ['PRC1_P', 'PRC1_Y']

def gen_SAVE_ALIGNMENT_VALUES(dof):
    class SAVE_ALIGNMENT_VALUES(GuardState):
        request = True
        @assert_mc_locked
        @assert_dof_locked_gen(dof)
        @get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def main(self):
            flag = []
            for i in range(len(self.optics)):            
                if ezca['GRD-SUS_%s_NOTIFICATION'%self.optics[i]]:
                    notify('SAVE %s'%self.optics[i])
                    flag = True
            if flag == False:
                return True
    return SAVE_ALIGNMENT_VALUES

SAVE_INPUT_ALIGNMENT = gen_SAVE_ALIGNMENT_VALUES('XARM')
SAVE_INPUT_ALIGNMENT.optics = ['PR2']

SAVE_YARM_ALIGNMENT = gen_SAVE_ALIGNMENT_VALUES('YARM')
SAVE_YARM_ALIGNMENT.optics = ['ITMY', 'ETMY']

##################################################
#camera servo, copies from IAS states

##################################################
# TMSX(Y) alignment using ITMX(Y) Camera
##################################################
# user should pre-define the following variables when making an instance
# yPosTarget
# xPosTarget
# arm
# xCal
# yCal
# mu
# GoodError
def gen_ALIGN_PR3(alsXCameraX,alsXCameraY, xPosTarget, yPosTarget, xCal, yCal,mu, GoodError, exp_time):
    class ALIGN_PR3(GuardState):
        request = False

        def main(self):
            # set variables
            self.alsXCameraX = alsXCameraX
            self.alsXCameraY = alsXCameraY
            self.xPosTarget = xPosTarget
            self.yPosTarget = yPosTarget
            self.xCal = xCal
            self.yCal = yCal
            self.mu = mu
            self.GoodError = GoodError

            # define some channels names and etc
            self.pr3TrampPit = 'SUS-PR3_M1_OPTICALIGN_P_TRAMP' 
            self.pr3TrampYaw = 'SUS-PR3_M1_OPTICALIGN_Y_TRAMP' 
            self.pr3BiasPit = 'SUS-PR3_M1_OPTICALIGN_P_OFFSET' 
            self.pr3BiasYaw = 'SUS-PR3_M1_OPTICALIGN_Y_OFFSET' 
            self.aslXCameraX = 'VID-CAM04_X'
            self.alsXCameraY = 'VID-CAM04_Y'

            # Set camera exposure
            ezca['VID-CAM04_EXP'] = exp_time

            # Set up ramping time to 1 sec before updating the offsets
            ezca[self.pr3TrampPit] = 1
            ezca[self.pr3TrampYaw] = 1

            # Initialize position error, pix, timer
            self.error = 10
            self.timer['PR3move'] = 0
            self.NoTRIALs = 0
            self.errorx = GigE_centering(self.pr3BiasYaw,       # actuation point
                                                 self.alsXCameraX,       # error signal
                                                 self.xPosTarget,       # target value
                                                 (self.mu)/(self.xCal)  # calibration and adoption rate
                                                  )
            self.errory = GigE_centering(self.pr3BiasPit,       # actuation point
                                                 self.alsXCameraY,       # error signal
                                                 self.yPosTarget,       # target value
                                                 (self.mu)/(self.yCal)  # calibration and adoption rate
                                                  )

        def run(self):
            if self.timer['PR3move']:  # Allow time for optic to move ( 1sec default)
                log('xerror is %f pixels' %self.errorx)
                log('yerror is %f pixels' %self.errory)
                # move the spot on camera 
                if abs(self.errorx) > self.GoodError*0.7:
                    log('moving pr3 yaw')
                    self.errorx = GigE_centering(self.pr3BiasYaw,       # actuation point
                                                 self.alsXCameraX,       # error signal
                                                 self.xPosTarget,       # target value
                                                 (self.mu)/(self.xCal)  # calibration and adoption rate
                                                  )
                if abs(self.errory) > self.GoodError*0.7:
                    log('moving pr3 pit')
                    self.errory = GigE_centering(self.pr3BiasPit,       # actuation point
                                                 self.alsXCameraY,       # error signal
                                                 self.yPosTarget,       # target value
                                                 (self.mu)/(self.yCal)  # calibration and adoption rate
                                                  )

                # Compute distance to the target
                self.error = sqrt( pow(self.errorx, 2) + pow(self.errory, 2))

                # Allow time for TMSX to move
                self.timer['PR3move'] = 1
                log('######### Current distance to the target is %.5f pixels' %
                    (self.error))
                self.NoTRIALs += 1

            # exit this state if error is smaller than 1 pixel
            if self.error <= self.GoodError:
                return True
            # exit when the pixel does not converge
            if self.NoTRIALs > 200:
                notify('ERROR: the number of iterations exceeded 200.')
                return 'FAULT'

    return ALIGN_PR3
# align TMSX
ALIGN_PR3 = gen_ALIGN_PR3(
            alsXCameraX = 'VID-CAM04_X',
            alsXCameraY = 'VID-CAM04_Y',
            xPosTarget = 389.3,
            yPosTarget = 236,
            xCal = -3.25,
            yCal = -9,
            mu = 0.01,
            GoodError = 2, # in pixels
            exp_time = 35000)


## TMSX aligned
class PR3_ALIGNED(GuardState):
    request = True
    def main(self):
        log('TMS aligned')
        pass
##################################################
##################################################

edges = [
# PRX edges:
    ('DOWN', 'SET_PRM_SRM'),
    ('SET_PRM_SRM','LOCK_PRX'),
    ('LOCK_PRX', 'REFL_WFS_CENTERING_PRX'), 
    ('REFL_WFS_CENTERING_PRX', 'PRM_ALIGN'),
    ('PRM_ALIGN', 'OFFLOAD_PRM_ALIGNMENT'), 
# SRY edges:
    ('SET_PRM_SRM', 'LOCK_SRY'),
    ('LOCK_SRY', 'REFL_WFS_CENTERING_SRY'), 
    ('REFL_WFS_CENTERING_SRY','SRM_ALIGN'),
    ('SRM_ALIGN', 'OFFLOAD_SRM_ALIGNMENT'),
    ('OFFLOAD_SRM_ALIGNMENT','OFFLOAD_SR2_ALIGNMENT'),
#XARM IR edges:
    ('DOWN', 'LOCK_XARM_IR'),
    ('LOCK_XARM_IR', 'REFL_WFS_CENTERING_XARM'),
    ('REFL_WFS_CENTERING_XARM', 'SET_INPUT_ALIGN'), 
    ('SET_INPUT_ALIGN', 'INPUT_ALIGN'),
    ('INPUT_ALIGN', 'OFFLOAD_INPUT_ALIGN'),
    ('OFFLOAD_INPUT_ALIGN', 'SAVE_INPUT_ALIGNMENT'),
    ('SAVE_INPUT_ALIGNMENT', 'LOCK_XARM_IR'),
# PR3 edges
    ('INPUT_ALIGN', 'ALIGN_PR3'),
    ('ALIGN_PR3', 'PR3_ALIGNED'),
    ('PR3_ALIGNED', 'INPUT_ALIGN'),
#Y ARM IR edges:
    ('DOWN', 'LOCK_YARM_IR'),
    ('LOCK_YARM_IR', 'REFL_WFS_CENTERING_YARM'), 
    ('REFL_WFS_CENTERING_YARM', 'YARM_ALIGN_IR'), 
    ('YARM_ALIGN_IR', 'SAVE_YARM_ALIGNMENT'), 
#PRMI edges
    ('DOWN', 'LOCK_PRMI'), 
    ('LOCK_PRMI', 'CORNER_WFS_CENTERING_PRMI')
    ]

##################################################
# SVN $Id: ISC_DOF.py 9911 2015-02-25 11:46:04Z kiwamu.izumi@LIGO.ORG $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/isc/h1/guardian/ISC_DOF.py $
##################################################

#class YARM_ALIGN_IR(GuardState):
#    request = True
#    @assert_mc_locked
#    @assert_yarm_IR_locked
#    @get_subordinate_watchdog_check_decorator(nodes)
#    @nodes.checker()
#    def main(self):
        ###### PARAMETERS ######
        # list of the error signals
#        self.errors = ('ASC-REFL_A_RF9_I_PIT_OUTMON', 'ASC-REFL_A_RF9_I_YAW_OUTMON', 'ASC-REFL_B_RF9_I_PIT_OUTMON', 'ASC-REFL_B_RF9_I_YAW_OUTMON')
#        # list of corresponding actuation points
#        self.actuators = ('SUS-ITMY_M0_OPTICALIGN_P_OFFSET', 'SUS-ITMY_M0_OPTICALIGN_Y_OFFSET', 'SUS-ETMY_M0_OPTICALIGN_P_OFFSET', 'SUS-ETMY_M0_OPTICALIGN_Y_OFFSET')
#        # loop gains 
#        self.gains = [-0.001, -0.001, -0.001, 0.001]
#        # power signal used to trigger
#        self.powersig = 'LSC-Y_TR_A_LF_OUTMON'
#
#       self.yarm_threshold = 5000   # do something only if power is larger than this
#        self.yarm_err_threshold = 5  # stop when all error signals are below this value#
#
#       # time constant for the running average of the error signals
#        self.T = 2*numpy.pi*10 
#        # initial error values (used for termination condition)
#        self.average_err = numpy.zeros(len(self.errors))
#        for i in range(len(self.errors)):
#            self.average_err[i] = ezca[self.errors[i]]
#        # compute the maximum of error signal absolute values
#        self.max_err = max(self.average_err)
#        # set ramp times
#        ezca['SUS-ETMY_M0_OPTICALIGN_P_TRAMP'] = 1
#        ezca['SUS-ETMY_M0_OPTICALIGN_Y_TRAMP'] = 1
#        ezca['SUS-ITMY_M0_OPTICALIGN_P_TRAMP'] = 1
#        ezca['SUS-ITMY_M0_OPTICALIGN_Y_TRAMP'] = 1

#    @assert_mc_locked
#    @assert_yarm_IR_locked
#    @get_subordinate_watchdog_check_decorator(nodes)
#    @nodes.checker()
#    def run(self):
#        t0 = time.time()
#        # compute new maximum value of error signal
#        self.max_err = max(abs(self.average_err))
#        # wait a little bit
#        time.sleep(0.01)
#        # Main loop, continue until error signals are all smaller than threshold. Stop if power drops below threshold
#        if self.max_err > self.yarm_err_threshold and ezca[self.powersig] > self.yarm_threshold:
#            # compute the time since the last actuation, to maintain uniform gain
#            dt = time.time() - t0
#            # loop over all error signals
#            for i in range(len(self.errors)):
#                # get error signal value
#                errsig = ezca[self.errors[i]]
#                # compute new position and apply it
#                ezca[self.actuators[i]] = ezca[self.actuators[i]] + self.gains[i] * dt * errsig
#                # running average on error signal value
#                self.average_err[i] = (1 - 1./self.T) * self.average_err[i] + 1./self.T * ezca[self.errors[i]]*errsig
#        else:
#            return True


